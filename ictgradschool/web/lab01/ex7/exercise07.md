Herbivorous:

  * Horse
  * Cow
  * Sheep
  * Capybara

Carnivorous:

  * Cat
  * Dog
  * Polar Bear
  * Frog
  * Hawk
  * Lion

Omnivorous:

  * Chicken
  * Raccoon
  * Fox
  * Rat
  